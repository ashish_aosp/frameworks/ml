// clang-format off
// Generated file (from: box_with_nms_limit.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace box_with_nms_limit {
// Generated box_with_nms_limit test
#include "generated/examples/box_with_nms_limit.example.cpp"
// Generated model constructor
#include "generated/models/box_with_nms_limit.model.cpp"
} // namespace box_with_nms_limit

TEST_F(GeneratedTests, box_with_nms_limit) {
    execute(box_with_nms_limit::CreateModel,
            box_with_nms_limit::is_ignored,
            box_with_nms_limit::get_examples());
}

TEST_F(GeneratedTests, box_with_nms_limit_relaxed) {
    execute(box_with_nms_limit::CreateModel_relaxed,
            box_with_nms_limit::is_ignored_relaxed,
            box_with_nms_limit::get_examples_relaxed());
}

TEST_F(GeneratedTests, box_with_nms_limit_float16) {
    execute(box_with_nms_limit::CreateModel_float16,
            box_with_nms_limit::is_ignored_float16,
            box_with_nms_limit::get_examples_float16());
}

TEST_F(GeneratedTests, box_with_nms_limit_quant8) {
    execute(box_with_nms_limit::CreateModel_quant8,
            box_with_nms_limit::is_ignored_quant8,
            box_with_nms_limit::get_examples_quant8());
}

#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape,
            box_with_nms_limit::is_ignored_dynamic_output_shape,
            box_with_nms_limit::get_examples_dynamic_output_shape());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_relaxed) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_relaxed,
            box_with_nms_limit::is_ignored_dynamic_output_shape_relaxed,
            box_with_nms_limit::get_examples_dynamic_output_shape_relaxed());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_float16) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_float16,
            box_with_nms_limit::is_ignored_dynamic_output_shape_float16,
            box_with_nms_limit::get_examples_dynamic_output_shape_float16());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_quant8) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_quant8,
            box_with_nms_limit::is_ignored_dynamic_output_shape_quant8,
            box_with_nms_limit::get_examples_dynamic_output_shape_quant8());
}

#endif
TEST_F(GeneratedTests, box_with_nms_limit_2) {
    execute(box_with_nms_limit::CreateModel_2,
            box_with_nms_limit::is_ignored_2,
            box_with_nms_limit::get_examples_2());
}

TEST_F(GeneratedTests, box_with_nms_limit_relaxed_2) {
    execute(box_with_nms_limit::CreateModel_relaxed_2,
            box_with_nms_limit::is_ignored_relaxed_2,
            box_with_nms_limit::get_examples_relaxed_2());
}

TEST_F(GeneratedTests, box_with_nms_limit_float16_2) {
    execute(box_with_nms_limit::CreateModel_float16_2,
            box_with_nms_limit::is_ignored_float16_2,
            box_with_nms_limit::get_examples_float16_2());
}

TEST_F(GeneratedTests, box_with_nms_limit_quant8_2) {
    execute(box_with_nms_limit::CreateModel_quant8_2,
            box_with_nms_limit::is_ignored_quant8_2,
            box_with_nms_limit::get_examples_quant8_2());
}

#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_2) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_2,
            box_with_nms_limit::is_ignored_dynamic_output_shape_2,
            box_with_nms_limit::get_examples_dynamic_output_shape_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_relaxed_2) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_relaxed_2,
            box_with_nms_limit::is_ignored_dynamic_output_shape_relaxed_2,
            box_with_nms_limit::get_examples_dynamic_output_shape_relaxed_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_float16_2) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_float16_2,
            box_with_nms_limit::is_ignored_dynamic_output_shape_float16_2,
            box_with_nms_limit::get_examples_dynamic_output_shape_float16_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, box_with_nms_limit_dynamic_output_shape_quant8_2) {
    execute(box_with_nms_limit::CreateModel_dynamic_output_shape_quant8_2,
            box_with_nms_limit::is_ignored_dynamic_output_shape_quant8_2,
            box_with_nms_limit::get_examples_dynamic_output_shape_quant8_2());
}

#endif
