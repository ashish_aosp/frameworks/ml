// clang-format off
// Generated file (from: slice.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace slice {
// Generated slice test
#include "generated/examples/slice.example.cpp"
// Generated model constructor
#include "generated/models/slice.model.cpp"
} // namespace slice

TEST_F(GeneratedTests, slice) {
    execute(slice::CreateModel,
            slice::is_ignored,
            slice::get_examples());
}

TEST_F(GeneratedTests, slice_relaxed) {
    execute(slice::CreateModel_relaxed,
            slice::is_ignored_relaxed,
            slice::get_examples_relaxed());
}

TEST_F(GeneratedTests, slice_float16) {
    execute(slice::CreateModel_float16,
            slice::is_ignored_float16,
            slice::get_examples_float16());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape) {
    execute(slice::CreateModel_dynamic_output_shape,
            slice::is_ignored_dynamic_output_shape,
            slice::get_examples_dynamic_output_shape());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed,
            slice::is_ignored_dynamic_output_shape_relaxed,
            slice::get_examples_dynamic_output_shape_relaxed());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16) {
    execute(slice::CreateModel_dynamic_output_shape_float16,
            slice::is_ignored_dynamic_output_shape_float16,
            slice::get_examples_dynamic_output_shape_float16());
}

#endif
TEST_F(GeneratedTests, slice_2) {
    execute(slice::CreateModel_2,
            slice::is_ignored_2,
            slice::get_examples_2());
}

TEST_F(GeneratedTests, slice_relaxed_2) {
    execute(slice::CreateModel_relaxed_2,
            slice::is_ignored_relaxed_2,
            slice::get_examples_relaxed_2());
}

TEST_F(GeneratedTests, slice_float16_2) {
    execute(slice::CreateModel_float16_2,
            slice::is_ignored_float16_2,
            slice::get_examples_float16_2());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_2) {
    execute(slice::CreateModel_dynamic_output_shape_2,
            slice::is_ignored_dynamic_output_shape_2,
            slice::get_examples_dynamic_output_shape_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_2) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_2,
            slice::is_ignored_dynamic_output_shape_relaxed_2,
            slice::get_examples_dynamic_output_shape_relaxed_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_2) {
    execute(slice::CreateModel_dynamic_output_shape_float16_2,
            slice::is_ignored_dynamic_output_shape_float16_2,
            slice::get_examples_dynamic_output_shape_float16_2());
}

#endif
TEST_F(GeneratedTests, slice_3) {
    execute(slice::CreateModel_3,
            slice::is_ignored_3,
            slice::get_examples_3());
}

TEST_F(GeneratedTests, slice_relaxed_3) {
    execute(slice::CreateModel_relaxed_3,
            slice::is_ignored_relaxed_3,
            slice::get_examples_relaxed_3());
}

TEST_F(GeneratedTests, slice_float16_3) {
    execute(slice::CreateModel_float16_3,
            slice::is_ignored_float16_3,
            slice::get_examples_float16_3());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_3) {
    execute(slice::CreateModel_dynamic_output_shape_3,
            slice::is_ignored_dynamic_output_shape_3,
            slice::get_examples_dynamic_output_shape_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_3) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_3,
            slice::is_ignored_dynamic_output_shape_relaxed_3,
            slice::get_examples_dynamic_output_shape_relaxed_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_3) {
    execute(slice::CreateModel_dynamic_output_shape_float16_3,
            slice::is_ignored_dynamic_output_shape_float16_3,
            slice::get_examples_dynamic_output_shape_float16_3());
}

#endif
TEST_F(GeneratedTests, slice_4) {
    execute(slice::CreateModel_4,
            slice::is_ignored_4,
            slice::get_examples_4());
}

TEST_F(GeneratedTests, slice_relaxed_4) {
    execute(slice::CreateModel_relaxed_4,
            slice::is_ignored_relaxed_4,
            slice::get_examples_relaxed_4());
}

TEST_F(GeneratedTests, slice_float16_4) {
    execute(slice::CreateModel_float16_4,
            slice::is_ignored_float16_4,
            slice::get_examples_float16_4());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_4) {
    execute(slice::CreateModel_dynamic_output_shape_4,
            slice::is_ignored_dynamic_output_shape_4,
            slice::get_examples_dynamic_output_shape_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_4) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_4,
            slice::is_ignored_dynamic_output_shape_relaxed_4,
            slice::get_examples_dynamic_output_shape_relaxed_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_4) {
    execute(slice::CreateModel_dynamic_output_shape_float16_4,
            slice::is_ignored_dynamic_output_shape_float16_4,
            slice::get_examples_dynamic_output_shape_float16_4());
}

#endif
TEST_F(GeneratedTests, slice_5) {
    execute(slice::CreateModel_5,
            slice::is_ignored_5,
            slice::get_examples_5());
}

TEST_F(GeneratedTests, slice_relaxed_5) {
    execute(slice::CreateModel_relaxed_5,
            slice::is_ignored_relaxed_5,
            slice::get_examples_relaxed_5());
}

TEST_F(GeneratedTests, slice_float16_5) {
    execute(slice::CreateModel_float16_5,
            slice::is_ignored_float16_5,
            slice::get_examples_float16_5());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_5) {
    execute(slice::CreateModel_dynamic_output_shape_5,
            slice::is_ignored_dynamic_output_shape_5,
            slice::get_examples_dynamic_output_shape_5());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_5) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_5,
            slice::is_ignored_dynamic_output_shape_relaxed_5,
            slice::get_examples_dynamic_output_shape_relaxed_5());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_5) {
    execute(slice::CreateModel_dynamic_output_shape_float16_5,
            slice::is_ignored_dynamic_output_shape_float16_5,
            slice::get_examples_dynamic_output_shape_float16_5());
}

#endif
TEST_F(GeneratedTests, slice_6) {
    execute(slice::CreateModel_6,
            slice::is_ignored_6,
            slice::get_examples_6());
}

TEST_F(GeneratedTests, slice_relaxed_6) {
    execute(slice::CreateModel_relaxed_6,
            slice::is_ignored_relaxed_6,
            slice::get_examples_relaxed_6());
}

TEST_F(GeneratedTests, slice_float16_6) {
    execute(slice::CreateModel_float16_6,
            slice::is_ignored_float16_6,
            slice::get_examples_float16_6());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_6) {
    execute(slice::CreateModel_dynamic_output_shape_6,
            slice::is_ignored_dynamic_output_shape_6,
            slice::get_examples_dynamic_output_shape_6());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_6) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_6,
            slice::is_ignored_dynamic_output_shape_relaxed_6,
            slice::get_examples_dynamic_output_shape_relaxed_6());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_6) {
    execute(slice::CreateModel_dynamic_output_shape_float16_6,
            slice::is_ignored_dynamic_output_shape_float16_6,
            slice::get_examples_dynamic_output_shape_float16_6());
}

#endif
TEST_F(GeneratedTests, slice_7) {
    execute(slice::CreateModel_7,
            slice::is_ignored_7,
            slice::get_examples_7());
}

TEST_F(GeneratedTests, slice_relaxed_7) {
    execute(slice::CreateModel_relaxed_7,
            slice::is_ignored_relaxed_7,
            slice::get_examples_relaxed_7());
}

TEST_F(GeneratedTests, slice_float16_7) {
    execute(slice::CreateModel_float16_7,
            slice::is_ignored_float16_7,
            slice::get_examples_float16_7());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_7) {
    execute(slice::CreateModel_dynamic_output_shape_7,
            slice::is_ignored_dynamic_output_shape_7,
            slice::get_examples_dynamic_output_shape_7());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_7) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_7,
            slice::is_ignored_dynamic_output_shape_relaxed_7,
            slice::get_examples_dynamic_output_shape_relaxed_7());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_7) {
    execute(slice::CreateModel_dynamic_output_shape_float16_7,
            slice::is_ignored_dynamic_output_shape_float16_7,
            slice::get_examples_dynamic_output_shape_float16_7());
}

#endif
TEST_F(GeneratedTests, slice_8) {
    execute(slice::CreateModel_8,
            slice::is_ignored_8,
            slice::get_examples_8());
}

TEST_F(GeneratedTests, slice_relaxed_8) {
    execute(slice::CreateModel_relaxed_8,
            slice::is_ignored_relaxed_8,
            slice::get_examples_relaxed_8());
}

TEST_F(GeneratedTests, slice_float16_8) {
    execute(slice::CreateModel_float16_8,
            slice::is_ignored_float16_8,
            slice::get_examples_float16_8());
}

#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_8) {
    execute(slice::CreateModel_dynamic_output_shape_8,
            slice::is_ignored_dynamic_output_shape_8,
            slice::get_examples_dynamic_output_shape_8());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_relaxed_8) {
    execute(slice::CreateModel_dynamic_output_shape_relaxed_8,
            slice::is_ignored_dynamic_output_shape_relaxed_8,
            slice::get_examples_dynamic_output_shape_relaxed_8());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, slice_dynamic_output_shape_float16_8) {
    execute(slice::CreateModel_dynamic_output_shape_float16_8,
            slice::is_ignored_dynamic_output_shape_float16_8,
            slice::get_examples_dynamic_output_shape_float16_8());
}

#endif
