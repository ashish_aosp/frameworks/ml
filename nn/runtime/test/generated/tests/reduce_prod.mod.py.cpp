// clang-format off
// Generated file (from: reduce_prod.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace reduce_prod {
// Generated reduce_prod test
#include "generated/examples/reduce_prod.example.cpp"
// Generated model constructor
#include "generated/models/reduce_prod.model.cpp"
} // namespace reduce_prod

TEST_F(GeneratedTests, reduce_prod) {
    execute(reduce_prod::CreateModel,
            reduce_prod::is_ignored,
            reduce_prod::get_examples());
}

TEST_F(GeneratedTests, reduce_prod_relaxed) {
    execute(reduce_prod::CreateModel_relaxed,
            reduce_prod::is_ignored_relaxed,
            reduce_prod::get_examples_relaxed());
}

TEST_F(GeneratedTests, reduce_prod_float16) {
    execute(reduce_prod::CreateModel_float16,
            reduce_prod::is_ignored_float16,
            reduce_prod::get_examples_float16());
}

#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape) {
    execute(reduce_prod::CreateModel_dynamic_output_shape,
            reduce_prod::is_ignored_dynamic_output_shape,
            reduce_prod::get_examples_dynamic_output_shape());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_relaxed) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_relaxed,
            reduce_prod::is_ignored_dynamic_output_shape_relaxed,
            reduce_prod::get_examples_dynamic_output_shape_relaxed());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_float16) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_float16,
            reduce_prod::is_ignored_dynamic_output_shape_float16,
            reduce_prod::get_examples_dynamic_output_shape_float16());
}

#endif
TEST_F(GeneratedTests, reduce_prod_2) {
    execute(reduce_prod::CreateModel_2,
            reduce_prod::is_ignored_2,
            reduce_prod::get_examples_2());
}

TEST_F(GeneratedTests, reduce_prod_relaxed_2) {
    execute(reduce_prod::CreateModel_relaxed_2,
            reduce_prod::is_ignored_relaxed_2,
            reduce_prod::get_examples_relaxed_2());
}

TEST_F(GeneratedTests, reduce_prod_float16_2) {
    execute(reduce_prod::CreateModel_float16_2,
            reduce_prod::is_ignored_float16_2,
            reduce_prod::get_examples_float16_2());
}

#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_2) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_2,
            reduce_prod::is_ignored_dynamic_output_shape_2,
            reduce_prod::get_examples_dynamic_output_shape_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_relaxed_2) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_relaxed_2,
            reduce_prod::is_ignored_dynamic_output_shape_relaxed_2,
            reduce_prod::get_examples_dynamic_output_shape_relaxed_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_float16_2) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_float16_2,
            reduce_prod::is_ignored_dynamic_output_shape_float16_2,
            reduce_prod::get_examples_dynamic_output_shape_float16_2());
}

#endif
TEST_F(GeneratedTests, reduce_prod_3) {
    execute(reduce_prod::CreateModel_3,
            reduce_prod::is_ignored_3,
            reduce_prod::get_examples_3());
}

TEST_F(GeneratedTests, reduce_prod_relaxed_3) {
    execute(reduce_prod::CreateModel_relaxed_3,
            reduce_prod::is_ignored_relaxed_3,
            reduce_prod::get_examples_relaxed_3());
}

TEST_F(GeneratedTests, reduce_prod_float16_3) {
    execute(reduce_prod::CreateModel_float16_3,
            reduce_prod::is_ignored_float16_3,
            reduce_prod::get_examples_float16_3());
}

#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_3) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_3,
            reduce_prod::is_ignored_dynamic_output_shape_3,
            reduce_prod::get_examples_dynamic_output_shape_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_relaxed_3) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_relaxed_3,
            reduce_prod::is_ignored_dynamic_output_shape_relaxed_3,
            reduce_prod::get_examples_dynamic_output_shape_relaxed_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_float16_3) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_float16_3,
            reduce_prod::is_ignored_dynamic_output_shape_float16_3,
            reduce_prod::get_examples_dynamic_output_shape_float16_3());
}

#endif
TEST_F(GeneratedTests, reduce_prod_4) {
    execute(reduce_prod::CreateModel_4,
            reduce_prod::is_ignored_4,
            reduce_prod::get_examples_4());
}

TEST_F(GeneratedTests, reduce_prod_relaxed_4) {
    execute(reduce_prod::CreateModel_relaxed_4,
            reduce_prod::is_ignored_relaxed_4,
            reduce_prod::get_examples_relaxed_4());
}

TEST_F(GeneratedTests, reduce_prod_float16_4) {
    execute(reduce_prod::CreateModel_float16_4,
            reduce_prod::is_ignored_float16_4,
            reduce_prod::get_examples_float16_4());
}

#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_4) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_4,
            reduce_prod::is_ignored_dynamic_output_shape_4,
            reduce_prod::get_examples_dynamic_output_shape_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_relaxed_4) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_relaxed_4,
            reduce_prod::is_ignored_dynamic_output_shape_relaxed_4,
            reduce_prod::get_examples_dynamic_output_shape_relaxed_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, reduce_prod_dynamic_output_shape_float16_4) {
    execute(reduce_prod::CreateModel_dynamic_output_shape_float16_4,
            reduce_prod::is_ignored_dynamic_output_shape_float16_4,
            reduce_prod::get_examples_dynamic_output_shape_float16_4());
}

#endif
