// clang-format off
// Generated file (from: sub_quantized_different_scales.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace sub_quantized_different_scales {
// Generated sub_quantized_different_scales test
#include "generated/examples/sub_quantized_different_scales.example.cpp"
// Generated model constructor
#include "generated/models/sub_quantized_different_scales.model.cpp"
} // namespace sub_quantized_different_scales

TEST_F(GeneratedTests, sub_quantized_different_scales) {
    execute(sub_quantized_different_scales::CreateModel,
            sub_quantized_different_scales::is_ignored,
            sub_quantized_different_scales::get_examples());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape,
            sub_quantized_different_scales::get_examples_dynamic_output_shape());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_2) {
    execute(sub_quantized_different_scales::CreateModel_2,
            sub_quantized_different_scales::is_ignored_2,
            sub_quantized_different_scales::get_examples_2());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_2) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_2,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_2,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_2());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_3) {
    execute(sub_quantized_different_scales::CreateModel_3,
            sub_quantized_different_scales::is_ignored_3,
            sub_quantized_different_scales::get_examples_3());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_3) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_3,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_3,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_3());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_4) {
    execute(sub_quantized_different_scales::CreateModel_4,
            sub_quantized_different_scales::is_ignored_4,
            sub_quantized_different_scales::get_examples_4());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_4) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_4,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_4,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_4());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_5) {
    execute(sub_quantized_different_scales::CreateModel_5,
            sub_quantized_different_scales::is_ignored_5,
            sub_quantized_different_scales::get_examples_5());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_5) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_5,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_5,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_5());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_6) {
    execute(sub_quantized_different_scales::CreateModel_6,
            sub_quantized_different_scales::is_ignored_6,
            sub_quantized_different_scales::get_examples_6());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_6) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_6,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_6,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_6());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_7) {
    execute(sub_quantized_different_scales::CreateModel_7,
            sub_quantized_different_scales::is_ignored_7,
            sub_quantized_different_scales::get_examples_7());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_7) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_7,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_7,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_7());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_8) {
    execute(sub_quantized_different_scales::CreateModel_8,
            sub_quantized_different_scales::is_ignored_8,
            sub_quantized_different_scales::get_examples_8());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_8) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_8,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_8,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_8());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_9) {
    execute(sub_quantized_different_scales::CreateModel_9,
            sub_quantized_different_scales::is_ignored_9,
            sub_quantized_different_scales::get_examples_9());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_9) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_9,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_9,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_9());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_10) {
    execute(sub_quantized_different_scales::CreateModel_10,
            sub_quantized_different_scales::is_ignored_10,
            sub_quantized_different_scales::get_examples_10());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_10) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_10,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_10,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_10());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_11) {
    execute(sub_quantized_different_scales::CreateModel_11,
            sub_quantized_different_scales::is_ignored_11,
            sub_quantized_different_scales::get_examples_11());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_11) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_11,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_11,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_11());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_12) {
    execute(sub_quantized_different_scales::CreateModel_12,
            sub_quantized_different_scales::is_ignored_12,
            sub_quantized_different_scales::get_examples_12());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_12) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_12,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_12,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_12());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_13) {
    execute(sub_quantized_different_scales::CreateModel_13,
            sub_quantized_different_scales::is_ignored_13,
            sub_quantized_different_scales::get_examples_13());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_13) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_13,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_13,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_13());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_14) {
    execute(sub_quantized_different_scales::CreateModel_14,
            sub_quantized_different_scales::is_ignored_14,
            sub_quantized_different_scales::get_examples_14());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_14) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_14,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_14,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_14());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_15) {
    execute(sub_quantized_different_scales::CreateModel_15,
            sub_quantized_different_scales::is_ignored_15,
            sub_quantized_different_scales::get_examples_15());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_15) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_15,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_15,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_15());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_16) {
    execute(sub_quantized_different_scales::CreateModel_16,
            sub_quantized_different_scales::is_ignored_16,
            sub_quantized_different_scales::get_examples_16());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_16) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_16,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_16,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_16());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_17) {
    execute(sub_quantized_different_scales::CreateModel_17,
            sub_quantized_different_scales::is_ignored_17,
            sub_quantized_different_scales::get_examples_17());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_17) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_17,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_17,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_17());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_18) {
    execute(sub_quantized_different_scales::CreateModel_18,
            sub_quantized_different_scales::is_ignored_18,
            sub_quantized_different_scales::get_examples_18());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_18) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_18,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_18,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_18());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_19) {
    execute(sub_quantized_different_scales::CreateModel_19,
            sub_quantized_different_scales::is_ignored_19,
            sub_quantized_different_scales::get_examples_19());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_19) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_19,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_19,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_19());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_20) {
    execute(sub_quantized_different_scales::CreateModel_20,
            sub_quantized_different_scales::is_ignored_20,
            sub_quantized_different_scales::get_examples_20());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_20) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_20,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_20,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_20());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_21) {
    execute(sub_quantized_different_scales::CreateModel_21,
            sub_quantized_different_scales::is_ignored_21,
            sub_quantized_different_scales::get_examples_21());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_21) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_21,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_21,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_21());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_22) {
    execute(sub_quantized_different_scales::CreateModel_22,
            sub_quantized_different_scales::is_ignored_22,
            sub_quantized_different_scales::get_examples_22());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_22) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_22,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_22,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_22());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_23) {
    execute(sub_quantized_different_scales::CreateModel_23,
            sub_quantized_different_scales::is_ignored_23,
            sub_quantized_different_scales::get_examples_23());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_23) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_23,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_23,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_23());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_24) {
    execute(sub_quantized_different_scales::CreateModel_24,
            sub_quantized_different_scales::is_ignored_24,
            sub_quantized_different_scales::get_examples_24());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_24) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_24,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_24,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_24());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_25) {
    execute(sub_quantized_different_scales::CreateModel_25,
            sub_quantized_different_scales::is_ignored_25,
            sub_quantized_different_scales::get_examples_25());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_25) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_25,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_25,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_25());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_26) {
    execute(sub_quantized_different_scales::CreateModel_26,
            sub_quantized_different_scales::is_ignored_26,
            sub_quantized_different_scales::get_examples_26());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_26) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_26,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_26,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_26());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_27) {
    execute(sub_quantized_different_scales::CreateModel_27,
            sub_quantized_different_scales::is_ignored_27,
            sub_quantized_different_scales::get_examples_27());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_27) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_27,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_27,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_27());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_28) {
    execute(sub_quantized_different_scales::CreateModel_28,
            sub_quantized_different_scales::is_ignored_28,
            sub_quantized_different_scales::get_examples_28());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_28) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_28,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_28,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_28());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_29) {
    execute(sub_quantized_different_scales::CreateModel_29,
            sub_quantized_different_scales::is_ignored_29,
            sub_quantized_different_scales::get_examples_29());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_29) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_29,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_29,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_29());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_30) {
    execute(sub_quantized_different_scales::CreateModel_30,
            sub_quantized_different_scales::is_ignored_30,
            sub_quantized_different_scales::get_examples_30());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_30) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_30,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_30,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_30());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_31) {
    execute(sub_quantized_different_scales::CreateModel_31,
            sub_quantized_different_scales::is_ignored_31,
            sub_quantized_different_scales::get_examples_31());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_31) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_31,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_31,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_31());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_32) {
    execute(sub_quantized_different_scales::CreateModel_32,
            sub_quantized_different_scales::is_ignored_32,
            sub_quantized_different_scales::get_examples_32());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_32) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_32,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_32,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_32());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_33) {
    execute(sub_quantized_different_scales::CreateModel_33,
            sub_quantized_different_scales::is_ignored_33,
            sub_quantized_different_scales::get_examples_33());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_33) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_33,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_33,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_33());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_34) {
    execute(sub_quantized_different_scales::CreateModel_34,
            sub_quantized_different_scales::is_ignored_34,
            sub_quantized_different_scales::get_examples_34());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_34) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_34,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_34,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_34());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_35) {
    execute(sub_quantized_different_scales::CreateModel_35,
            sub_quantized_different_scales::is_ignored_35,
            sub_quantized_different_scales::get_examples_35());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_35) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_35,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_35,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_35());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_36) {
    execute(sub_quantized_different_scales::CreateModel_36,
            sub_quantized_different_scales::is_ignored_36,
            sub_quantized_different_scales::get_examples_36());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_36) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_36,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_36,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_36());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_37) {
    execute(sub_quantized_different_scales::CreateModel_37,
            sub_quantized_different_scales::is_ignored_37,
            sub_quantized_different_scales::get_examples_37());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_37) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_37,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_37,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_37());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_38) {
    execute(sub_quantized_different_scales::CreateModel_38,
            sub_quantized_different_scales::is_ignored_38,
            sub_quantized_different_scales::get_examples_38());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_38) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_38,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_38,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_38());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_39) {
    execute(sub_quantized_different_scales::CreateModel_39,
            sub_quantized_different_scales::is_ignored_39,
            sub_quantized_different_scales::get_examples_39());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_39) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_39,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_39,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_39());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_40) {
    execute(sub_quantized_different_scales::CreateModel_40,
            sub_quantized_different_scales::is_ignored_40,
            sub_quantized_different_scales::get_examples_40());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_40) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_40,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_40,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_40());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_41) {
    execute(sub_quantized_different_scales::CreateModel_41,
            sub_quantized_different_scales::is_ignored_41,
            sub_quantized_different_scales::get_examples_41());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_41) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_41,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_41,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_41());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_42) {
    execute(sub_quantized_different_scales::CreateModel_42,
            sub_quantized_different_scales::is_ignored_42,
            sub_quantized_different_scales::get_examples_42());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_42) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_42,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_42,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_42());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_43) {
    execute(sub_quantized_different_scales::CreateModel_43,
            sub_quantized_different_scales::is_ignored_43,
            sub_quantized_different_scales::get_examples_43());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_43) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_43,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_43,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_43());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_44) {
    execute(sub_quantized_different_scales::CreateModel_44,
            sub_quantized_different_scales::is_ignored_44,
            sub_quantized_different_scales::get_examples_44());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_44) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_44,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_44,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_44());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_45) {
    execute(sub_quantized_different_scales::CreateModel_45,
            sub_quantized_different_scales::is_ignored_45,
            sub_quantized_different_scales::get_examples_45());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_45) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_45,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_45,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_45());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_46) {
    execute(sub_quantized_different_scales::CreateModel_46,
            sub_quantized_different_scales::is_ignored_46,
            sub_quantized_different_scales::get_examples_46());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_46) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_46,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_46,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_46());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_47) {
    execute(sub_quantized_different_scales::CreateModel_47,
            sub_quantized_different_scales::is_ignored_47,
            sub_quantized_different_scales::get_examples_47());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_47) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_47,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_47,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_47());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_48) {
    execute(sub_quantized_different_scales::CreateModel_48,
            sub_quantized_different_scales::is_ignored_48,
            sub_quantized_different_scales::get_examples_48());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_48) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_48,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_48,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_48());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_49) {
    execute(sub_quantized_different_scales::CreateModel_49,
            sub_quantized_different_scales::is_ignored_49,
            sub_quantized_different_scales::get_examples_49());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_49) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_49,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_49,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_49());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_50) {
    execute(sub_quantized_different_scales::CreateModel_50,
            sub_quantized_different_scales::is_ignored_50,
            sub_quantized_different_scales::get_examples_50());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_50) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_50,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_50,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_50());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_51) {
    execute(sub_quantized_different_scales::CreateModel_51,
            sub_quantized_different_scales::is_ignored_51,
            sub_quantized_different_scales::get_examples_51());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_51) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_51,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_51,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_51());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_52) {
    execute(sub_quantized_different_scales::CreateModel_52,
            sub_quantized_different_scales::is_ignored_52,
            sub_quantized_different_scales::get_examples_52());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_52) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_52,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_52,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_52());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_53) {
    execute(sub_quantized_different_scales::CreateModel_53,
            sub_quantized_different_scales::is_ignored_53,
            sub_quantized_different_scales::get_examples_53());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_53) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_53,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_53,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_53());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_54) {
    execute(sub_quantized_different_scales::CreateModel_54,
            sub_quantized_different_scales::is_ignored_54,
            sub_quantized_different_scales::get_examples_54());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_54) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_54,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_54,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_54());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_55) {
    execute(sub_quantized_different_scales::CreateModel_55,
            sub_quantized_different_scales::is_ignored_55,
            sub_quantized_different_scales::get_examples_55());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_55) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_55,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_55,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_55());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_56) {
    execute(sub_quantized_different_scales::CreateModel_56,
            sub_quantized_different_scales::is_ignored_56,
            sub_quantized_different_scales::get_examples_56());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_56) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_56,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_56,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_56());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_57) {
    execute(sub_quantized_different_scales::CreateModel_57,
            sub_quantized_different_scales::is_ignored_57,
            sub_quantized_different_scales::get_examples_57());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_57) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_57,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_57,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_57());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_58) {
    execute(sub_quantized_different_scales::CreateModel_58,
            sub_quantized_different_scales::is_ignored_58,
            sub_quantized_different_scales::get_examples_58());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_58) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_58,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_58,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_58());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_59) {
    execute(sub_quantized_different_scales::CreateModel_59,
            sub_quantized_different_scales::is_ignored_59,
            sub_quantized_different_scales::get_examples_59());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_59) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_59,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_59,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_59());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_60) {
    execute(sub_quantized_different_scales::CreateModel_60,
            sub_quantized_different_scales::is_ignored_60,
            sub_quantized_different_scales::get_examples_60());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_60) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_60,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_60,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_60());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_61) {
    execute(sub_quantized_different_scales::CreateModel_61,
            sub_quantized_different_scales::is_ignored_61,
            sub_quantized_different_scales::get_examples_61());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_61) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_61,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_61,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_61());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_62) {
    execute(sub_quantized_different_scales::CreateModel_62,
            sub_quantized_different_scales::is_ignored_62,
            sub_quantized_different_scales::get_examples_62());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_62) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_62,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_62,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_62());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_63) {
    execute(sub_quantized_different_scales::CreateModel_63,
            sub_quantized_different_scales::is_ignored_63,
            sub_quantized_different_scales::get_examples_63());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_63) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_63,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_63,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_63());
}

#endif
TEST_F(GeneratedTests, sub_quantized_different_scales_64) {
    execute(sub_quantized_different_scales::CreateModel_64,
            sub_quantized_different_scales::is_ignored_64,
            sub_quantized_different_scales::get_examples_64());
}

#if 0
TEST_F(DynamicOutputShapeTests, sub_quantized_different_scales_dynamic_output_shape_64) {
    execute(sub_quantized_different_scales::CreateModel_dynamic_output_shape_64,
            sub_quantized_different_scales::is_ignored_dynamic_output_shape_64,
            sub_quantized_different_scales::get_examples_dynamic_output_shape_64());
}

#endif
