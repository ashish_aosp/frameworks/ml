// clang-format off
// Generated file (from: log_softmax.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace log_softmax {
// Generated log_softmax test
#include "generated/examples/log_softmax.example.cpp"
// Generated model constructor
#include "generated/models/log_softmax.model.cpp"
} // namespace log_softmax

TEST_F(GeneratedTests, log_softmax) {
    execute(log_softmax::CreateModel,
            log_softmax::is_ignored,
            log_softmax::get_examples());
}

TEST_F(GeneratedTests, log_softmax_relaxed) {
    execute(log_softmax::CreateModel_relaxed,
            log_softmax::is_ignored_relaxed,
            log_softmax::get_examples_relaxed());
}

TEST_F(GeneratedTests, log_softmax_float16) {
    execute(log_softmax::CreateModel_float16,
            log_softmax::is_ignored_float16,
            log_softmax::get_examples_float16());
}

#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape) {
    execute(log_softmax::CreateModel_dynamic_output_shape,
            log_softmax::is_ignored_dynamic_output_shape,
            log_softmax::get_examples_dynamic_output_shape());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_relaxed) {
    execute(log_softmax::CreateModel_dynamic_output_shape_relaxed,
            log_softmax::is_ignored_dynamic_output_shape_relaxed,
            log_softmax::get_examples_dynamic_output_shape_relaxed());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_float16) {
    execute(log_softmax::CreateModel_dynamic_output_shape_float16,
            log_softmax::is_ignored_dynamic_output_shape_float16,
            log_softmax::get_examples_dynamic_output_shape_float16());
}

#endif
TEST_F(GeneratedTests, log_softmax_2) {
    execute(log_softmax::CreateModel_2,
            log_softmax::is_ignored_2,
            log_softmax::get_examples_2());
}

TEST_F(GeneratedTests, log_softmax_relaxed_2) {
    execute(log_softmax::CreateModel_relaxed_2,
            log_softmax::is_ignored_relaxed_2,
            log_softmax::get_examples_relaxed_2());
}

TEST_F(GeneratedTests, log_softmax_float16_2) {
    execute(log_softmax::CreateModel_float16_2,
            log_softmax::is_ignored_float16_2,
            log_softmax::get_examples_float16_2());
}

#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_2) {
    execute(log_softmax::CreateModel_dynamic_output_shape_2,
            log_softmax::is_ignored_dynamic_output_shape_2,
            log_softmax::get_examples_dynamic_output_shape_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_relaxed_2) {
    execute(log_softmax::CreateModel_dynamic_output_shape_relaxed_2,
            log_softmax::is_ignored_dynamic_output_shape_relaxed_2,
            log_softmax::get_examples_dynamic_output_shape_relaxed_2());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_float16_2) {
    execute(log_softmax::CreateModel_dynamic_output_shape_float16_2,
            log_softmax::is_ignored_dynamic_output_shape_float16_2,
            log_softmax::get_examples_dynamic_output_shape_float16_2());
}

#endif
TEST_F(GeneratedTests, log_softmax_3) {
    execute(log_softmax::CreateModel_3,
            log_softmax::is_ignored_3,
            log_softmax::get_examples_3());
}

TEST_F(GeneratedTests, log_softmax_relaxed_3) {
    execute(log_softmax::CreateModel_relaxed_3,
            log_softmax::is_ignored_relaxed_3,
            log_softmax::get_examples_relaxed_3());
}

TEST_F(GeneratedTests, log_softmax_float16_3) {
    execute(log_softmax::CreateModel_float16_3,
            log_softmax::is_ignored_float16_3,
            log_softmax::get_examples_float16_3());
}

#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_3) {
    execute(log_softmax::CreateModel_dynamic_output_shape_3,
            log_softmax::is_ignored_dynamic_output_shape_3,
            log_softmax::get_examples_dynamic_output_shape_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_relaxed_3) {
    execute(log_softmax::CreateModel_dynamic_output_shape_relaxed_3,
            log_softmax::is_ignored_dynamic_output_shape_relaxed_3,
            log_softmax::get_examples_dynamic_output_shape_relaxed_3());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_float16_3) {
    execute(log_softmax::CreateModel_dynamic_output_shape_float16_3,
            log_softmax::is_ignored_dynamic_output_shape_float16_3,
            log_softmax::get_examples_dynamic_output_shape_float16_3());
}

#endif
TEST_F(GeneratedTests, log_softmax_4) {
    execute(log_softmax::CreateModel_4,
            log_softmax::is_ignored_4,
            log_softmax::get_examples_4());
}

TEST_F(GeneratedTests, log_softmax_relaxed_4) {
    execute(log_softmax::CreateModel_relaxed_4,
            log_softmax::is_ignored_relaxed_4,
            log_softmax::get_examples_relaxed_4());
}

TEST_F(GeneratedTests, log_softmax_float16_4) {
    execute(log_softmax::CreateModel_float16_4,
            log_softmax::is_ignored_float16_4,
            log_softmax::get_examples_float16_4());
}

#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_4) {
    execute(log_softmax::CreateModel_dynamic_output_shape_4,
            log_softmax::is_ignored_dynamic_output_shape_4,
            log_softmax::get_examples_dynamic_output_shape_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_relaxed_4) {
    execute(log_softmax::CreateModel_dynamic_output_shape_relaxed_4,
            log_softmax::is_ignored_dynamic_output_shape_relaxed_4,
            log_softmax::get_examples_dynamic_output_shape_relaxed_4());
}

#endif
#if 0
TEST_F(DynamicOutputShapeTests, log_softmax_dynamic_output_shape_float16_4) {
    execute(log_softmax::CreateModel_dynamic_output_shape_float16_4,
            log_softmax::is_ignored_dynamic_output_shape_float16_4,
            log_softmax::get_examples_dynamic_output_shape_float16_4());
}

#endif
